from django.shortcuts import render, redirect 
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import TaskForm

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task_instance = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    taskz = Task.objects.filter(assignee=request.user)
    context = {
        "taskz": taskz,
    }
    return render(request, "tasks/mine.html", context)
